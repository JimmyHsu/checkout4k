from time import time
from selenium.webdriver.chrome.options import Options
from selenium import webdriver
import urllib.request
import os
from selenium.webdriver.common.by import By
from tensorflow import keras
from PIL import Image
from os import listdir
import tensorflow as tf
import numpy as np 
from datetime import datetime, timedelta
import smtplib
import ssl
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email import encoders
from selenium.webdriver.common.keys import Keys
import time
def lfunc(fdir, td, leavetimeoutter, index) :
    img_height = 40
    img_width = 22
    chromedriver = "C:\\Users\\jimmy.hsu\\Desktop\\chromedriver"
    opt = Options()
    opt.add_experimental_option("debuggerAddress", "localhost:9222")
    driver = webdriver.Chrome(chromedriver, chrome_options=opt)  
    URL = "http://tw-compbase.supermicro.com:6699/"
    driver.get(URL)
    ###########################is record #######################################
    print("------------------------------------------time----------------------------------")
    try :
        try :
            isrecord = driver.find_element(by=By.XPATH, value='/html/body/form/div[3]/div[2]/div[2]/table/tbody/tr[5]/td/table/tbody/tr/td').text
        except :
            isrecord = driver.find_element(by=By.XPATH, value='//*[@id="log"]/tbody/tr/td').text

    except :
       isrecord = "check in has beeen done"
   
        

    #############################################################################
    ###########################check in time record #######################################
    try :
        try :
            stime = driver.find_element(by=By.XPATH, value='//*[@id="log"]/tbody/tr[2]/td[1]').text
        except :
            stime = driver.find_element(by=By.XPATH, value='/html/body/form/div[3]/div[2]/div[2]/table/tbody/tr[5]/td/table/tbody/tr[2]/td[1]').text
    except :
        stime = "no check-in record"   
    #######################################################################################
    ###########################check out time record ######################################
    try :
        try : 
            leavetime = driver.find_element(by=By.XPATH, value='/html/body/form/div[3]/div[2]/div[2]/table/tbody/tr[7]/td/table/tbody/tr[2]/td[2]').text
        except :
            leavetime = driver.find_element(by=By.XPATH, value='//*[@id="log"]/tbody/tr[2]/td[2]').text
    except :
        leavetime = 'no check-out record'    

    #######################################################################################
   
   
    ##############################change it for production #####################################
    if leavetime == "" and leavetimeoutter == "" and datetime.now().time() >= estleavetime  : 
    ############################################################################################
        os.mkdir(fdir+"\\1")
        os.mkdir(fdir+"\\1\\test")
        imgURL =driver.find_element(by=By.ID, value="ImgCaptcha").get_attribute("src")
        urllib.request.urlretrieve(imgURL, fdir +"\\{}.jpg".format( td))
        model2 = keras.models.load_model('C:\\Users\\jimmy.hsu\\Desktop\\Project\\leavecheck\\my-model.h5')
        print("model loaded")
        ant = []
        batch_size = 64
        train_ds = tf.keras.utils.image_dataset_from_directory(
        "C:\\Users\\jimmy.hsu\\Desktop\\train data\\clean data\\",
            
        validation_split=0.2,
        subset="training",
        seed=123,
        image_size=(img_height, img_width),
        batch_size=batch_size)
        class_names = train_ds.class_names
        print(class_names)
        for filename in listdir(fdir) :
            if filename not in ['1'] :
                im = Image.open(fdir +"\\{}.jpg".format( td))
                width, height = im.size
                for i in range(5) :
                    if i == 3  :
                        image = im.crop(((i*width/5)-5, 0 , ((i+1)*width/5)-5, height)).convert('RGB').save(fdir+"\\1\\test\\img.jpg")
                            
                    elif i ==4 :
                        image =im.crop(((i*width/5)-5, 0 , ((i+1)*width/5)-5, height)).convert('RGB').save(fdir+"\\1\\test\\img.jpg")
                    elif i ==1 :
                        image =im.crop(((i*width/5)-3, 0 , ((i+1)*width/5)-3, height)).convert('RGB').save(fdir+"\\1\\test\\img.jpg")


                    else :
                        image =im.crop((i*width/5, 0 , (i+1)*width/5, height)).convert('RGB').save(fdir+"\\1\\test\\img.jpg")

                    test = tf.keras.utils.image_dataset_from_directory(
                        fdir+"\\1",
                            
                        image_size=(img_height, img_width)
                        )
                    
                    predictions = model2.predict(test)
                    score = tf.nn.softmax(predictions[0])
                        
                    ant.append(class_names[np.argmax(score)])    
            print(ant)
  
        ############### For Test ################################################
        #URL = "https://www.google.com.tw/?hl=zh_TW"
        #driver.get(URL)
        
        #m = driver.find_element(by=By.NAME, value="q")
        #string = ''
        #for item in ant :
        #    string = string+item
        #m.send_keys(string)
        #m.send_keys(Keys.RETURN)
        #URL = "https://www.google.com.tw/?hl=zh_TW"
        #driver.get(URL)
        #time.sleep(3)
        #leavetime = driver.find_element(by=By.NAME, value="q").get_attribute("value")
        #########################################################################

        ############### For Real ################################################
        m = driver.find_element(by=By.NAME, value="captchacode")
        string = ''
        for item in ant :
            string = string+item
        m.send_keys(string)
        m.send_keys(Keys.RETURN)
        URL = "http://tw-compbase.supermicro.com:6699/"
        driver.get(URL)
        time.sleep(10)
        try :
           leavetime = driver.find_element(by=By.XPATH, value='//*[@id="log"]/tbody/tr[2]/td[2]').text
        except : 
           leavetime = driver.find_element(by=By.XPATH, value='/html/body/form/div[3]/div[2]/div[2]/table/tbody/tr[7]/td/table/tbody/tr[2]/td[2]').text

        ##########################################################################
        print('Left at {}'.format(leavetime))
        waittime = 0
    
        
    return leavetime , waittime